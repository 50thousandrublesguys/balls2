package view 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	import model.Config;
	/**
	 * ...
	 * @author Eugene Kolesnikov aka Mitd
	 */
	public class Console extends EventDispatcher
	{
		private var _view:Sprite;
		
		private var _levelTF:TextField;
		private var _ringSpeedTF:TextField;
		private var _gameTime:TextField;
		private var _closeButton:Sprite;
		
		private var _ballSpeed:TextField;
		private var _maxAngle:TextField;
		
		public function Console(view:Sprite) 
		{
			_view = view;
			(_view["l0"] as TextField).text = "Ring speed:";
			_ringSpeedTF = _view["tf0"] as TextField;
			_ringSpeedTF.text = ""+Config.RING_SPEED;
			_ringSpeedTF.addEventListener(Event.CHANGE, handleTextChange);
			
			(_view["l1"] as TextField).text = "Game time:";
			_gameTime = _view["tf1"] as TextField;
			_gameTime.text = ""+Config.MAX_TIMER;
			_gameTime.addEventListener(Event.CHANGE, handleTextChange);
			
			(_view["l2"] as TextField).text = "Next level:";
			_levelTF = _view["tf2"] as TextField;
			_levelTF.text = ""+Config.CURRENT_LEVEL;
			_levelTF.addEventListener(Event.CHANGE, handleTextChange);
			
			_closeButton = _view["closeButton"] as Sprite;
			_closeButton.addEventListener(MouseEvent.CLICK, handleCloseClick);
			
			
			(_view["l3"] as TextField).text = "Ball move time:";
			_ballSpeed = _view["tf3"] as TextField;
			_ballSpeed.text = ""+Config.BALL_STEP_TIME;
			_ballSpeed.addEventListener(Event.CHANGE, handleTextChange);
			
			(_view["l4"] as TextField).text = "Angle btw Enter and Basket:";
			_maxAngle = _view["tf4"] as TextField;
			_maxAngle.text = ""+Config.MAX_ANGLE;
			_maxAngle.addEventListener(Event.CHANGE, handleTextChange);
		}
		
		private function handleCloseClick(e:MouseEvent): void
		{
			visible = false;
		}
		
		private function handleTextChange(e:Event):void 
		{
			(e.target as TextField).removeEventListener(Event.CHANGE, handleTextChange);
			
			var text:TextField = e.target as TextField;
			var value:Number = Number(text.text);
			switch(text)
			{
				case _ringSpeedTF:
					Config.RING_SPEED = isNaN(value) ? Config.RING_SPEED : value;
					_ringSpeedTF.text = ""+Config.RING_SPEED;
					break;
				case _gameTime:
					Config.MAX_TIMER = isNaN(value) ? Config.MAX_TIMER : value;
					_gameTime.text = ""+Config.MAX_TIMER;
					break;
				case _levelTF:
					Config.CURRENT_LEVEL = (int(value) < 0) ? 0 : int(value);
					_levelTF.text = ""+Config.CURRENT_LEVEL;
					break;
				case _ballSpeed:
					var speed:Number = Number(value);
					if(isNaN(speed))
					{
						speed = Config.BALL_STEP_TIME;
					}
					if(speed == 0)
					{
						speed = 0.1;
					}
					if(speed > 0.3)
					{
						speed = 0.3;
					}
					Config.BALL_STEP_TIME = speed;
					_ballSpeed.text = ""+Config.BALL_STEP_TIME;
					break;
				case _maxAngle:
					Config.MAX_ANGLE = isNaN(value) ? Config.MAX_ANGLE : ((value < 0) ? 0 : value);
					_maxAngle.text = ""+Config.MAX_ANGLE;
					break;
			}
			
			(e.target as TextField).addEventListener(Event.CHANGE, handleTextChange);
		}
		
		public function set visible(value:Boolean) : void
		{
			_view.visible = value;
		}
		
		public function get visible() : Boolean
		{
			return _view.visible;
		}
	}

}