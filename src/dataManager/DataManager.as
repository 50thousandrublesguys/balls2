﻿package dataManager
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;

	public class DataManager extends EventDispatcher
	{
		private static const TRIM_PATTERN:RegExp = /^\s+|\s+$/g;
		public static var LEVEL:String	= "level";
		
		public static const READY:String = "READY";
		
		private var _dataXML:XML;
		private var _levelData:Vector.<Level>;
		private var _ringModels:Array;
		
		public function DataManager()
		{
		}
		
		public function load() : void
		{
			initDB();
		}
		
		private function initDB ():void
		{
			var file:File = File.applicationDirectory.resolvePath("data.xml"); 
			var fileStream:FileStream = new FileStream();
			fileStream.open(file, FileMode.READ);
			_dataXML = XML(fileStream.readUTFBytes(fileStream.bytesAvailable));
			parseDB();
		}
		
		private function parseDB():void
		{
			//first ring models then levels
			_ringModels = [];
			var models:XMLList = _dataXML.stages.places;
			var places:XML;
			var ringModel:RingModel;
			for each(places in models)
			{
				ringModel = new RingModel(places);
				_ringModels[ringModel.id] = ringModel;
			}
			
			_levelData = new Vector.<Level>;
			var levels:XMLList = _dataXML.difficulty.level;
			var level:Level;
			for each (var el:XML in levels)
			{
				level = new Level(el);
				level.ringModel = getRingModel(level.positionsInside);
				_levelData.push(level);
			}
			
			dispatchEvent(new Event(READY));
		}
		
		private function getRingModel(entryPoints:int):RingModel
		{
			return _ringModels[entryPoints];
		}
		
		public function getLevel(id:int) : Level
		{
			if(id >= _levelData.length)
			{
				id = _levelData.length - 1;
			}
			return _levelData[id];
		}
	}
}