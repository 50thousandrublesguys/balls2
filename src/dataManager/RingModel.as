﻿package dataManager 
{
	import flash.utils.Dictionary;
	
	import model.Edge;
	import model.Graph;
	import model.Vertex;

	/**
	 * ...
	 * @author Eugene "Mitd" Kolesnikov
	 */
	public class RingModel 
	{
		private var _graph:Graph;
		private var _vertexies:Array;
		private var _id:int;
		private var _positions:Array = [];
		
		public function RingModel(places:XML) 
		{
			var i:int;
			var j:int;
			
			_id = int(places.@id);
			var near:Array;
			var str:String;
			var enter:*;
			for each(var p:XML in places.place)
			{
				near = [];
				str = String(p.@near);
				if (str.length > 0)
				{
					near = str.split(",");
				}
				enter = int(p.@enter);
				_positions[int(p.@id)] = {near:near, way:int(p.@way), enter:Boolean(enter)};
			}
			
			_graph = new Graph();
			_vertexies = [];
			var vertex:Vertex;
			for(i = 0; i<_positions.length; i++)
			{
				if(_positions[i] != null)
				{
					vertex = new Vertex(""+i);
					_vertexies[i] = vertex;
					_graph.addVertex(vertex);
				}
			}
			

			var edge:Edge;
			var edges:Array = [];
			var o:Object;
			for(i = 0; i<_positions.length; i++)
			{
				if(_positions[i] != null)
				{
					o = _positions[i];
					for each(j in o.near)
					{
						if(i < j)
						{
							edge = new Edge(_vertexies[i], _vertexies[j], 10);
							edges.push(edge);
						}
						else
						{
							edge = new Edge(_vertexies[j], _vertexies[i], 10);
							edges.push(edge);
						}
					}
				}
			}
			
			//bad practice
			for(i = 0; i<edges.length; i++)
			{
				for(j = 0; j<edges.length; j++)
				{
					if(j != i)
					{
						if(edges[i].begin == edges[j].begin && edges[i].end == edges[j].end)
						{
							edges.splice(j, 1);
							j--;
							if(i > j)
							{
								i--;
							}
						}
					}
				}
			}
			
			for each(edge in edges)
			{
				edge.register();
				_graph.addEdge(edge);
			}
		}
	
		public function get vertexies():Array
		{
			return _vertexies;
		}

		public function get graph():Graph
		{
			return _graph;
		}

		public function get id():int 
		{
			return _id;
		}
		
		public function get positions():Array 
		{
			return _positions;
		}
		
	}

}