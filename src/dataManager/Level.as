package dataManager
{
	public class Level
	{
		public var id:int;
		public var entryPoints:int;
		public var newBallPositions:int;
		public var differentColors:int;
		public var positionsInside:int;
		public var rotatingSpeed:Number;
		public var levelDuration:int;
		public var ringModel:RingModel;
		
		public function Level(source:XML)
		{
			id 					= int(source.level);
			entryPoints 		= int(source.number_of_entry_points);
			newBallPositions 	= int(source.number_of_new_ball_positions);
			differentColors 	= int(source.number_of_different_colors);
			//see data.xml/place="id"
			positionsInside 	= int(source.ball_positions_inside_carrousel);
			rotatingSpeed	 	= Number(source.rotating_speed_of_carrousel);
			levelDuration	 	= int(source.level_duration);
		}
	}
}