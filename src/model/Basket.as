package model
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;

	public class Basket extends EventDispatcher
	{
		public static const EVENT_BASKET_CLICK:String = "EVENT_BASKET_CLICK";
		
		private var _view:Sprite;
		private var _indicator:Sprite;
		private var _color:int = -1;
		
		public function Basket()
		{
		}
		
		public function select() : void
		{
			_indicator.visible = true;
		}
		
		public function unselect():void 
		{
			_indicator.visible = false;
		}

		public function get view():Sprite
		{
			return _view;
		}

		public function set view(value:Sprite):void
		{
			_view = value;
			_view.addEventListener(MouseEvent.CLICK, handleMouseClick);
			_indicator = _view["indicator"];
			_indicator.visible = false;
			if (_color > 0)
			{
				applyColor();
			}
		}
		
		public function get color():int 
		{
			return _color;
		}
		
		public function set color(value:int):void 
		{
			_color = value;
			applyColor();
		}
		
		private function applyColor() : void
		{
			if (_view)
			{
				_view["bg"].gotoAndStop(_color);
			}
		}
		
		private function handleMouseClick(e:MouseEvent):void 
		{
			dispatchEvent(new Event(EVENT_BASKET_CLICK));
		}

	}
}