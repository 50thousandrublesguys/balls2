package model
{
	public class Config
	{
		static public const MAX_DIST:Number = 75;
		static public const MAX_BASKET_DIST:Number = 90;
		private static var _MAX_TIMER:int = 45000;
		//дистанция зачета шарика
		private static var _RING_SPEED:Number = 0.1;
		public static var CURRENT_LEVEL:int = 0;
		public static var USES_COLORS:Array = [];
		static private var _MAX_ANGLE:Number = 0.1;
		static private var _BALL_STEP_TIME:Number = 0.2;

		public static function get MAX_ANGLE():Number
		{
			return _MAX_ANGLE;
		}

		public static function set MAX_ANGLE(value:Number):void
		{
			if(value > 0.1)
			{
				value = 0.1;
			}
			_MAX_ANGLE = value;
		}

		public static function get MAX_TIMER():int
		{
			return _MAX_TIMER;
		}

		public static function set MAX_TIMER(value:int):void
		{
			_MAX_TIMER = ((value < 5) ? 5 : value);
		}

		public static function get BALL_STEP_TIME():Number
		{
			return _BALL_STEP_TIME;
		}

		public static function set BALL_STEP_TIME(value:Number):void
		{
			_BALL_STEP_TIME = value;
		}

		public static function get RING_SPEED():Number
		{
			return _RING_SPEED;
		}

		public static function set RING_SPEED(value:Number):void
		{
			_RING_SPEED = ((value > 1) ? 1 : value);
		}

	}
}