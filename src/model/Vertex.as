﻿package model
{
	import flash.display.Sprite;
	import flash.geom.Point;
	//
	public class Vertex
	{
		public var edges:/*Edge*/Array;
		public var name:String;
		public var index:int;
		//
		function Vertex (name:String) { 
			edges = []; this.name = name;
		}
	}
}