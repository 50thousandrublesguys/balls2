﻿package model
{
	import flash.utils.getTimer;

	public class GraphMethods
	{
		/**
		 * Method return the shortest path between two vertices in the graph. 
		 * The search is performed using the Dijkstra's algorithm.
		 * 
		 * @param	graph 		<Graph>					Graph with vertices and edges
		 * @param	begin 		<Vertex>				First peak
		 * @param	end				<Vertex>				Second peak
		 * @return					<Array>					Shortest path between two vertices
		 */
		public static function getDijkstra (graph:Graph, begin:Vertex, end:Vertex):Array
		{
			var t:Number = getTimer();
			
			var str:String;
			var edgeCost:Number;
			var edgesNum:int;
			//
			var edgeCosts:Array;
			var way:Array;
			var crr:Array;
			var drr:Array;
			//
			var i:int;
			var j:int;
			var currentVertexIndex:int;
			var numVertexes:int;
			//
			var beginVertexIndex:int;
			var endVertexIndex:int;
			//
			var vertexes:Array;
			var edges:Array;
			var edge:Edge;
			//
			var min:Function;
			//
			min = function (value:int):int
			{
				var res:int;
				//
				for (var i:int = 0; i < value; i++)
					if (!way[i]) res = i;
				//
				for (i = 0; i < value; i++)
					if ((crr[res] > crr[i]) && (!way[i])) res = i;
				//
				return res;
			}
			//
			vertexes = graph.vertexs;
			edges = graph.edges;
			//
			numVertexes = vertexes.length;
			//
			edgeCosts = [];
			way = [];
			crr = [];
			drr = [];
			//
			for (i = 0; i < numVertexes; i++)
			{
				edgeCosts[i] = [];
				for (j = 0; j < numVertexes; j++)
				{
					edgeCosts[i][j] = 0;
				}
			}
			//[numVertexes][numVertexes] = 0;
			edgesNum = edges.length;
			for (i = 0; i < edgesNum; i++)
			{
				edge = edges[i];
				if (edge)
				{
					beginVertexIndex = edge.begin.index;
					endVertexIndex = edge.end.index;
					//
					edgeCost = Math.round(edge.distanse);
					//
					edgeCosts[beginVertexIndex][endVertexIndex] = edgeCost;
					edgeCosts[endVertexIndex][beginVertexIndex] = edgeCost;
				}
			}
			//тем, у кого edge.distance == 0 выставляют 65535
			for (i = 0; i < numVertexes; i++)
			{
				for (j = 0; j < numVertexes; j++)
				{
					if (edgeCosts[i][j] == 0)
					{
						edgeCosts[i][j] = 65535;
					}
				}
			}
			//
			beginVertexIndex = begin.index;
			endVertexIndex = end.index;
			//
			if (beginVertexIndex == endVertexIndex) 
			{
				trace("PAHTFINDING TIME:", getTimer() - t);
				return null;
			}
			//
			for (i = 0; i < numVertexes; i++ )
			{
				way[i] = 0;
				crr[i] = 65535;
			}
			//
			crr[beginVertexIndex] = 0;
			way[beginVertexIndex] = 1;
			currentVertexIndex = beginVertexIndex;
			//
			for (i = 0; i < numVertexes; i++)
			{
				drr[i] = "";
				drr[i] += beginVertexIndex;
			}
			//
			do
			{
				for (i = 0; i < numVertexes; i++)
				{
					if ((edgeCosts[currentVertexIndex][i] != 65535) && (!way[i]) && (i != currentVertexIndex))
					{
						edgeCost = crr[currentVertexIndex] + edgeCosts[currentVertexIndex][i];
						if (crr[i] > edgeCost)
						{
							drr[i] = drr[currentVertexIndex];
							drr[i] += "-"
							drr[i] += i;
						}
						crr[i] = Math.min(crr[i], edgeCost)
					}
				}
				currentVertexIndex = min(numVertexes);
				way[currentVertexIndex] = 1;
			}
			while (currentVertexIndex != endVertexIndex);
			
			if (crr[currentVertexIndex])
			{
				str = drr[currentVertexIndex];
				if (str)
				{
					edgeCosts = str.split("-");
					if (edgeCosts)
					{
						way = [];
						edgesNum = edgeCosts.length;
						for (i = 0; i < edgesNum; i++)
						{
							way.push(vertexes[Number(edgeCosts[i])]);
						}
						trace("PAHTFINDING TIME:", getTimer() - t);
						return way;
					}
				}
			}
			trace("PAHTFINDING TIME:", getTimer() - t);
			return null;
		}
	}
}