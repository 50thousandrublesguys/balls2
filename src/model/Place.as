package model
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.text.TextField;

	public class Place extends EventDispatcher
	{
		public static const EVENT_PLACE_CLICK:String = "EVENT_PLACE_CLICK";
		
		public var id:int;
		public var near:Array;
		public var way:int;
		public var enter:Boolean = false;
		
		private var _view:Sprite;
		private var _indicator:Sprite;
		private var _holder:Ball;
		
		public function Place()
		{
		}
		
		public function select() : void {
			_indicator.visible = true;
		}
		
		public function unselect():void 
		{
			_indicator.visible = false;
		}

		public function get view():Sprite
		{
			return _view;
		}

		public function set view(value:Sprite):void
		{
			_view = value;
			_indicator = _view["indicator"];
			_indicator.visible = false;
			_view.addEventListener(MouseEvent.CLICK, handleClick);
		}
		
		public function get holder():Ball 
		{
			return _holder;
		}
		
		public function set holder(value:Ball):void 
		{
			_holder = value;
		}
		
		private function handleClick(e:MouseEvent):void 
		{
			dispatchEvent(new Event(EVENT_PLACE_CLICK));
		}

	}
}