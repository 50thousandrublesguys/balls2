﻿/**
 * @author Shamsutdinov Farid
 * @copyright Metaquotes Software Corp. © 2007-2009
 */
package model
{
	public class Graph
	{
		private var vertex_arr:Array;
		private var edge_arr:Array;
		/**
		 * 
		 */
		function Graph() 
		{
			vertex_arr = [];
			edge_arr = [];
		}
		/**
		 * 
		 */
		public function addVertex (vertex:Vertex):void
		{
			vertex_arr.push(vertex);
			vertex.index = vertex_arr.length - 1;
		}
		//
		public function addEdge (edge:Edge):void
		{
			edge_arr.push(edge);
		}
		/**
		 * 
		 */
		public function get vertexs ():Array
		{
			return vertex_arr;
		}
		//
		public function get edges ():Array
		{
			return edge_arr;
		}
		/**
		 * 
		 */
		public function get vertexNum ():int
		{
			return vertex_arr.length;
		}
		//
		public function get edgeNum ():int
		{
			return edge_arr.length;
		}
	}
}