﻿package model
{
	public class Edge
	{
		public var begin:Vertex;
		public var end:Vertex;
		public var distanse:Number;
		
		public function Edge (begin:Vertex = null, end:Vertex = null, distanse:Number = 0) 
		{
			this.begin = begin;
			this.end = end;
			this.distanse = distanse;
		}
		
		public function register() : void
		{
			begin.edges.push(this);
			end.edges.push(this);
		}
	}
}