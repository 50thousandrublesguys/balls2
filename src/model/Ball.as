package model
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.geom.Point;

	public class Ball extends EventDispatcher
	{
		public static const EVENT_CLICK:String = "EVENT_CLICK";
		
		private var _startPosition:int;
		
		private var _indicator:Sprite;
		private var _view:Sprite;
		private var _color:int;
		
		public var place:Place;
		
		public function Ball(ballColor:Number = -1)
		{
			//pick random color
			if(ballColor == -1)
			{
				var i:int = Math.random() * ColorEnum.colors.length;
				ballColor = ColorEnum.colors[i];
			}
			_color = ballColor;
		}
		
		public function unselect():void 
		{
			_indicator.visible = false;
		}
		
		public function dispose():void 
		{
			unselect();
			if(place)
			{
				place.holder = null;
				place = null;
			}
			if (_view)
			{
				_view.removeEventListener(MouseEvent.CLICK, handleClick);
				if (_view.parent)
				{
					_view.parent.removeChild(_view);
				}
				_view = null;
			}
		}
		
		public function getPos():Point
		{
			var pos:Point;
			if (_view)
			{
				pos = new Point(_view.x, _view.y);
			}
			return pos;
		}

		public function get view():Sprite
		{
			return _view;
		}

		public function set view(value:Sprite):void
		{
			_view = value;
			_view["bg"].gotoAndStop(_color);
			_view.addEventListener(MouseEvent.CLICK, handleClick, false, 0, true);
			_indicator = _view["indicator"];
			_indicator.visible = false;
		}
		
		public function get color():int 
		{
			return _color;
		}
		
		public function get startPosition():int 
		{
			return _startPosition;
		}
		
		public function set startPosition(value:int):void 
		{
			_startPosition = value;
		}
		
		private function handleClick(e:MouseEvent):void 
		{
			_indicator.visible = !_indicator.visible;
			dispatchEvent(new Event(EVENT_CLICK));
		}
		
		public function update(delta:Number) : void
		{
			
		}

	}
}