﻿package model
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.display.MovieClip;

	public class Ring
	{
		private var _view:Sprite;
		private var _places:Array;
		
		public function Ring()
		{
		}

		public function get view():Sprite
		{
			return _view;
		}
		public function set view(value:Sprite):void
		{
			_view = value;
		}
		
		public function defineInsidePlaces(id:int):Array
		{
			(_view as MovieClip).gotoAndStop(id);
			definePlaces();
			return _places;
		}
		
		private function definePlaces():void
		{
			var i:int = 0;
			var child:DisplayObject;
			var place:Place;
			var index:int;
			_places = [];
			while (i < _view.numChildren)
			{
				child = _view.getChildAt(i);
				if (child.name && child.name.indexOf("place_") != -1)
				{
					index = int(child.name.substring(child.name.indexOf("place_") + 6));
					place = new Place();
					place.id = index;
					place.view = child as Sprite;
					_places[place.id] = place;
				}
				i++;
			}
		}
	}
}