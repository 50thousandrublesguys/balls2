﻿package model
{
	public class ColorEnum
	{
		//ball.gotoAndStop(ColorEnum.colors[index])
		public static const colors : Array = [1, 2, 3, 4, 5, 6];
		
		public static function getRandomColors(count:int) : Array
		{
			var selectedColors:Array = [];
			
			if(count == colors.length)
			{
				selectedColors = selectedColors.concat(colors);
			}
			else
			{
				var colorsCopy:Array = [];
				colorsCopy = colorsCopy.concat(colors);
				
				var index:int = Math.random() * colorsCopy.length;
				while(count > 0)
				{
					selectedColors.push(colorsCopy.splice(index, 1)[0]);
					index = Math.random() * colorsCopy.length;
					count--;
				}
			}
			return selectedColors;
		}
	}
}