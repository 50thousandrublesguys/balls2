﻿package
{
	import flash.desktop.DockIcon;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Vector3D;
	import flash.text.TextField;
	import flash.utils.Dictionary;
	import flash.utils.clearTimeout;
	import flash.utils.getDefinitionByName;
	import flash.utils.getTimer;
	import flash.utils.setTimeout;
	
	import caurina.transitions.Tweener;
	
	import dataManager.DataManager;
	import dataManager.Level;
	import dataManager.RingModel;
	
	import fl.motion.easing.Sine;
	import fl.transitions.Tween;
	
	import model.Ball;
	import model.Basket;
	import model.ColorEnum;
	import model.Config;
	import model.Edge;
	import model.Graph;
	import model.GraphMethods;
	import model.Place;
	import model.Ring;
	import model.Vertex;
	
	import view.Console;
	
	public class RollBalls extends Sprite
	{
		private var _dataManager:DataManager;
		private var _currentLevel:Level;
		
		private var _timerTF:TextField;
		private var _scoreTF:TextField;
		private var _scores:int = 0;
		
		private var _lastTime:int = -1;
		private var _timer:int = 0;
		
		private var _consoleButton:Sprite;
		private var _console:Console;
		
		private var _ballsContainer:Sprite;
		private var _startBalls:Vector.<Ball>;
		private var _freeBalls:Vector.<Ball> = new Vector.<Ball>();
		
		private var _selectedBasket:Basket;
		private var _selectedBall:Ball;
		private var _selectedPlace:Place;
		
		//start places
		private var _ballPositions:Vector.<Sprite>;
		//places inside ring
		private var _insidePlaces:Array;
		//corner's baskets
		private var _baskets:Vector.<Basket>;
		private var _basketRun:Boolean = false;
		private var _ring:Ring;
		
		private var _enterPlaces:Array;
		
		private var _firstJumpTimeout:uint;
		private var _basketJumpTimeout:uint;
		
		public function RollBalls()
		{
			if(stage)
			{
				init(null);				
			}
			else
			{
				addEventListener(Event.ADDED_TO_STAGE, init);
			}
		}
		
		protected function init(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			
			_timerTF = this["timerTF"] as TextField;
			_scoreTF = this["scoreTF"] as TextField;
			
			_console = new Console(this["console"]);
			_console.visible = false;
			_consoleButton = this["consoleButton"] as Sprite;
			_consoleButton.addEventListener(MouseEvent.CLICK, handleConsoleClick);
			
			_ring = new Ring();
			_ring.view = this["ring"];
			
			_dataManager = new DataManager();
			_dataManager.addEventListener(DataManager.READY, dataManagerReady);
			_dataManager.load();
		}
		
		private function dataManagerReady(e:Event) : void
		{
			startGame();
		}
		
		private function handleConsoleClick(e:MouseEvent):void 
		{
			_console.visible = !_console.visible;
		}
		
		private function resetGame() : void
		{
			_basketRun = false;
			//омг!
			var i:int = 0;
			if(_ring.view)
			{
				var child:DisplayObject;
				while(i < _ring.view.numChildren)
				{
					child = _ring.view.getChildAt(i);
					if(child.name == "ball")
					{
						if(child.parent)
						{
							child.parent.removeChild(child);
						}
					}
					else
					{
						i++;	
					}
				}
			}
			
			if(_firstJumpTimeout)
			{
				clearTimeout(_firstJumpTimeout);
			}
			if(_basketJumpTimeout)
			{
				clearTimeout(_basketJumpTimeout);
			}
			Tweener.removeAllTweens();
			removeEventListener(Event.ENTER_FRAME, update);
			
			if(_selectedBall)
			{
				removeBall(_selectedBall)
				_selectedBall = null;
			}
			if(_selectedBasket)
			{
				_selectedBasket = null;
			}
			_selectedPlace = null;
			
			for each(var basket:Basket in _baskets)
			{
				basket.unselect();
			}
			
			//re-init gameobjects
			_enterPlaces = [];
			var ball:Ball;
			if (_insidePlaces)
			{
				for each(var place:Place in _insidePlaces)
				{
					if(place)
					{
						place.removeEventListener(Place.EVENT_PLACE_CLICK, handlePlaceClick);
						place.unselect();
					}
				}
			}
			for each(ball in _startBalls)
			{
				removeBall(ball);
			}
			_startBalls = new Vector.<Ball>();
			for each(ball in _freeBalls)
			{
				removeBall(ball);
			}
			_freeBalls = new Vector.<Ball>();
			
			_scores = 0;
			_scoreTF.text = "Score: "+_scores;
			_lastTime = -1;
			_timer = 0;
			
			Config.CURRENT_LEVEL++;
			startGame();
		}
		
		private function startGame() : void
		{
			//get level data
			_currentLevel = _dataManager.getLevel(Config.CURRENT_LEVEL);
			Config.MAX_TIMER = _currentLevel.levelDuration * 1000;
			Config.RING_SPEED = _currentLevel.rotatingSpeed;
			//подтягиваем цвета
			Config.USES_COLORS = ColorEnum.getRandomColors(_currentLevel.differentColors);
			//расставляем стартовые позиции
			defineBallStartPositions();
			//расставляем позиции
			defineInsidePositions();
			//расставляем корзины
			defineBaskets();
			//делаем шарики
			createBalls();
			//стартуем игру
			addEventListener(Event.ENTER_FRAME, update, false, 0, true);
		}
		
		private function defineBaskets():void 
		{
			var color:Number;
			var basket:Basket;
			var startColors:Array = [];
			startColors = startColors.concat(Config.USES_COLORS);
			var i:int = 1;
			var first:Boolean = false;
			if(!_baskets)
			{
				_baskets = new Vector.<Basket>();
				first = true;
			}
			while (i <= 4)
			{
				if(startColors.length > 0)
				{
					color = startColors.shift();
				}
				else
				{
					color = getRandomColor();
				}
				if(!first)
				{
					basket = _baskets[i-1];
				}
				else
				{
					basket = new Basket();
					basket.addEventListener(Basket.EVENT_BASKET_CLICK, handleBasketClick);
					basket.view = this["basket_" + i];
					_baskets.push(basket);
				}
				basket.color = color;
				i++;
			}
		}
		private function recolorizeBaskets() : void
		{
			if(Config.USES_COLORS.length <= _baskets.length)
			{
				return;	
			}
			
			var d:Dictionary = new Dictionary();
			for each(var ball:Ball in _freeBalls)
			{
				if(ball)
				{
					d[ball.color] = ball.color;
				}
			}
			var colors:Array = [];
			for each(var c:Number in d)
			{
				colors.push(c);				
			}
			for(var i:int = 0; i<_baskets.length; i++)
			{
				_baskets[i].color = colors[i];
			}
		}
		
		private function getRandomColor() : Number
		{
			var color:Number;
			var index:int = int(Config.USES_COLORS.length * Math.random());
			color = Config.USES_COLORS[index];
			return color;
		}
		private function handleBasketClick(e:Event):void 
		{
			if(_basketRun)
			{
				return;
			}
			
			if (!_selectedBall || _selectedBall.startPosition >= 0)
			{
				return;
			}
			if((e.target as Basket).color != _selectedBall.color)
			{
				return;
			}
			
			if (_selectedBasket)
			{
				_selectedBasket.unselect();
			}
			
			_selectedBasket = (e.target as Basket);
			getOptimalPath(_selectedBasket, _selectedBall);
		}
		
		private function getPath(ballPlace:Place, place:Place) : Vector.<Place>
		{
			var path:Vector.<Place> = new Vector.<Place>();
			var wayId:int = place.way;
			var nextPlace:Place;
			var p:Place;
			var ep:Place;
			
			if(ballPlace.way == wayId)
			{
				path.push(place);
			}
			else if(ballPlace.way == 0)
			{
				//идем в нужный путь
				for each(var epIndex:int in _enterPlaces)
				{
					ep = _insidePlaces[epIndex];
					if(ep.way == wayId)
					{
						p = ep;
						break;
					}
				}
				if(p)
				{
					if(wayId == 1)
					{
						path.push(_insidePlaces[p.id+1] as Place);
						path.push(p);
					}
					else
					{
						path.push(_insidePlaces[p.id-1] as Place);
						path.push(p);
					}
				}
			}
			else
			{
				if(ballPlace.way == 1)
				{
					if(ballPlace.enter)
					{
						path.push(_insidePlaces[ballPlace.id+1]);
						path.push(_insidePlaces[ballPlace.id+2]);
					}
					else
					{
						path.push(_insidePlaces[ballPlace.id+1]);
					}
				}
				else
				{
					if(ballPlace.enter)
					{
						path.push(_insidePlaces[ballPlace.id-1]);
						path.push(_insidePlaces[ballPlace.id-2]);
					}
					else
					{
						path.push(_insidePlaces[ballPlace.id-1]);
					}
				}
				path = path.concat(getPath(path[path.length-1] as Place, place));
			}
			return path;
		}
		
		private function getOptimalPath(basket:Basket, ball:Ball, simulate:Boolean = false ) : Boolean
		{
			var pos:Point = new Point(basket.view.x, basket.view.y);
			var ringModel:RingModel = _currentLevel.ringModel;
			var p:Array;
			var place:Place;
			var dW:Number;
			var path:Vector.<Place>;
			var founded:Boolean = false;
			var dist:Number;
			var global:Point;
			
			if(ball.place.enter)
			{
				var tmpPlace:Place = getNearestPlace(pos, Config.MAX_BASKET_DIST);
				if(ball.place == tmpPlace)
				{
					//мы рядышком - пусть составлять не надо
					if(checkPlaceAngle(pos, ball.place))
					{
						if (!simulate)
						{
							basket.select();
							//отвязываем шарик
							if(ball.place)
							{
								ball.place.holder = null;
							}
							ball.place = null;
							//флаг
							_basketRun = true;
							//побежали
							var newPos:Point = ball.view.parent.localToGlobal(ball.getPos());
							//дооолго меняет позицию сцуко
							_ballsContainer.addChild(ball.view);
							ball.view.x = newPos.x;
							ball.view.y = newPos.y;
							//телепортируем в корзину
							Tweener.addTween(ball.view, { time:Config.BALL_STEP_TIME, x: basket.view.x, y:basket.view.y, onComplete:ballInBasket, onCompleteParams:[ball, basket] } );
						}
						return true;
					}	
					return false;
				}
			}
			
			//for each(var placeIndex:int in _enterPlaces)
			//{
				//place = _insidePlaces[placeIndex];
				//path = getPath(ball.place, place);
				//if(path && checkPath(path))
				//{
					////изменение угла, которое произойдёт пока шарик добежит до выходного отверстия
					//dW = (path.length - 1) * Config.BALL_STEP_TIME * Config.RING_SPEED;
					//if(checkPlaceAngle(pos, place, dW))
					//{
						//trace("enter", placeIndex, "path:", path.toString());
						//founded = true;
						//break;
					//}
				//}
			//}
			
			for each(var placeIndex:int in _enterPlaces)
			{
				place = _insidePlaces[placeIndex];
				p = GraphMethods.getDijkstra(ringModel.graph, ringModel.vertexies[ball.place.id], ringModel.vertexies[place.id]);
				path = convertIndexesToPlaces(p);
				if(path && checkPath(path))
				{
					//изменение угла, которое произойдёт пока шарик добежит до выходного отверстия
					dW = (path.length - 1) * Config.BALL_STEP_TIME * Config.RING_SPEED;
					if(checkPlaceAngle(pos, place, dW))
					{
						founded = true;
						break;
					}
				}
			}
			
			if(founded)
			{
				if (!simulate)
				{
					basket.select();
					//отвязываем шарик
					if(ball.place)
					{
						ball.place.holder = null;
					}
					ball.place = null;
					//флаг
					_basketRun = true;
					//побежали
					runBall(ball, path, 0, basket);
				}
				return true;
			}
			else 
			{
				return false;
			}
		}
		
		private function defineBallStartPositions():void 
		{
			var pos:Sprite;
			_ballPositions = new Vector.<Sprite>();
			if (_ballPositions)
			{
				for each(pos in _ballPositions)
				{
					pos.visible = false;
					pos["indicator"].visible = false;
				}
			}
			
			for (var i:int = 1; i <= 12; i++)
			{
				this["start_"+i].visible = false;
				this["start_"+i]["indicator"].visible = false;
				if(i <= _currentLevel.newBallPositions)
				{
					this["start_"+i].visible = true;
					_ballPositions.push(this["start_" + i] as Sprite);
				}
			}
		}
		
		private function defineInsidePositions():void 
		{
			_enterPlaces = [];
			_insidePlaces = _ring.defineInsidePlaces(_currentLevel.positionsInside);
			var near:Object;
			for each(var place:Place in _insidePlaces)
			{
				if(place)
				{
					near = _currentLevel.ringModel.positions[place.id];
					place.near = near.near;
					place.way = near.way;
					place.enter = Boolean(near.enter);
					if(place.enter)
					{
						_enterPlaces.push(place.id);
					}
					place.addEventListener(Place.EVENT_PLACE_CLICK, handlePlaceClick);
				}
			}
		}
		
		private function handlePlaceClick(e:Event):void 
		{
			if (!_selectedBall)
			{
				return;
			}
			
			if (_selectedPlace)
			{
				_selectedPlace.unselect();
			}
			if (_selectedPlace && (_selectedPlace == e.target))
			{
				_selectedPlace = null;
			}
			else
			{
				_selectedPlace = e.target as Place;
				var ringModel:RingModel = _currentLevel.ringModel;
				var path:Vector.<Place>;
				var p:Array;
				if (_selectedBall.startPosition >= 0)
				{
					if(!_selectedBall.getPos())
					{
						return;
					}
					//смотрим можем ли мы заскочить в окружность
					var place:Place = getNearestPlace(_selectedBall.getPos());
					//если поблизости нашелся кто-либо
					if (place)
					{
						//смотрим угол между прямыми проведёнными через центр окружности и центры ball и place
						if (checkPlaceAngle(_selectedBall.getPos(), place))
						{
							if(place == _selectedPlace)
							{
								path = new Vector.<Place>();
								path.push(place);
							}
							else
							{
								//совпали - прокладываем путь дальше, если преград нет - мячик бежит
								p = GraphMethods.getDijkstra(ringModel.graph, ringModel.vertexies[place.id], ringModel.vertexies[_selectedPlace.id]);
								path = convertIndexesToPlaces(p);
							}
							if(path && checkPath(path))
							{
								_selectedPlace.select();
								if(_selectedBall.view.parent != _ring.view)
								{
									var newPos:Point = _selectedPlace.view.globalToLocal(_selectedBall.getPos());
									//дооолго меняет позицию сцуко
									_ring.view.addChild(_selectedBall.view);
									_selectedBall.view.x = newPos.x;
									_selectedBall.view.y = newPos.y;
									
									recteateBallAtStart(_selectedBall.startPosition);
									_selectedBall.startPosition = -1;
								}
								//в путь
								registerPlace(_selectedBall, _selectedPlace);
								runBall(_selectedBall, path);
							}
						}
					}
				}
				else
				{
					p = GraphMethods.getDijkstra(ringModel.graph, ringModel.vertexies[_selectedBall.place.id], ringModel.vertexies[_selectedPlace.id]);
					path = convertIndexesToPlaces(p);
					if(path && checkPath(path))
					{
						_selectedPlace.select();
						registerPlace(_selectedBall, _selectedPlace);
						runBall(_selectedBall, path);
					}
				}
			}
		}
		
		private function registerPlace(ball:Ball, place:Place) : void
		{
			if(ball.place)
			{
				ball.place.holder = null;
			}
			ball.place = null;
			ball.place = place;
			place.holder = ball;
		}
		
		private function convertIndexesToPlaces(arr:Array) : Vector.<Place>
		{
			var vec:Vector.<Place>;
			if(arr && arr.length > 0)
			{
				vec = new Vector.<Place>();
				var place:Place;
				for(var i:int = 0; i<arr.length; i++)
				{
					place = _insidePlaces[int(arr[i].name)];
					vec.push(place);
				}
			}
			return vec;
		}
		private function checkPath(path:Vector.<Place>) : Boolean
		{
			var isOk:Boolean = true;
			for(var i:int = 0; i<path.length; i++)
			{
				if(path[i].holder && _selectedBall != path[i].holder)
				{
					isOk = false;
					break;
				}
			}
			return isOk;
		}
		
		private function getNearestPlace(position:Point, minDist = Config.MAX_DIST) : Place {
			var global:Point;
			var minDist:Number = minDist;
			var dis:Number;
			var nearestPlace:Place;
			var place:Place;
			for each(var placeIndex:int in _enterPlaces)
			{
				place = _insidePlaces[placeIndex];
				if(place)
				{
					global = place.view.parent.localToGlobal(new Point(place.view.x, place.view.y));	
					dis = Point.distance(global, position);
					if (dis < minDist)
					{
						minDist = dis;
						nearestPlace = place;
					}
				}
			}
			return nearestPlace;
		}
		
		private function checkPlaceAngle(position:Point, place:Place, angleDelta:Number = 0) : Boolean
		{
			var pp:Point = new Point(place.view.x, place.view.y);
			//повернём на угол, если надо
			if(angleDelta > 0)
			{
				pp.x = pp.x * Math.cos(angleDelta) - pp.y * Math.sin(angleDelta);
				pp.y = pp.x * Math.sin(angleDelta) + pp.y * Math.cos(angleDelta);
			}
			var global:Point = place.view.parent.localToGlobal(pp);
			var zeroPoint:Vector3D = new Vector3D(_ring.view.x, _ring.view.y);
			var placePoint:Vector3D = new Vector3D(global.x, global.y);
			var ballPoint:Vector3D = new Vector3D(position.x, position.y);
			//нашли вектора
			var ballV:Vector3D = ballPoint.subtract(zeroPoint);
			var placeV:Vector3D = placePoint.subtract(zeroPoint);
			//теперь ищем угол между ними
			var angle:Number = Vector3D.angleBetween(ballV, placeV);
			trace("angle", angle);
//			angle = Math.atan2((ballV.x * placeV.y - placeV.x * ballV.y), (ballV.x * placeV.x + ballV.y * placeV.y));
//			trace("angle", angle);
			return (Math.abs(angle) <= Config.MAX_ANGLE);
		}
		
		private function runBall(ball:Ball, path:Vector.<Place>, index:int = 0, basket:Basket = null) : void
		{
			//trace("runBall", path.length, index, ball.view.x, ball.view.y);
			//ball.view.x = path[index].view.x;
			//ball.view.y = path[index].view.y;
			//if (index + 1 < path.length)
			//{
				//setTimeout(runBall, Config.BALL_STEP_TIME, ball, path, index + 1, basket);
			//}
			//else
			//{
				//setTimeout(ballRunComplete, Config.BALL_STEP_TIME, ball, path[index], basket);
			//}
			if(!ball || !ball.view)
			{
				return;
			}
			
			if(index+1 < path.length)
			{

				Tweener.addTween(ball.view, { time: Config.BALL_STEP_TIME, x:path[index].view.x, y:path[index].view.y, onComplete: runBall, onCompleteParams:[ball, path, index + 1, basket]});	
			}
			else
			{
				if (path[index].enter && !_basketRun)
				{
					ball.view.x = path[index].view.x;
					ball.view.y = path[index].view.y;
					_firstJumpTimeout = setTimeout(ballRunComplete, Config.BALL_STEP_TIME, ball, path[index], basket);
				}
				else
				{
					Tweener.addTween(ball.view, {time: Config.BALL_STEP_TIME, x:path[index].view.x, y:path[index].view.y, onComplete: ballRunComplete, onCompleteParams:[ball, path[index], basket]});
				}
			}
		}

		private function ballRunComplete(ball:Ball, place:Place, basket:Basket = null) : void
		{
			if(_basketRun)
			{
				if(ball && basket && ball.view && ball.view.parent)
				{
//					_basketJumpTimeout = setTimeout(goToBasket, Config.BALL_STEP_TIME * 1.5, ball, place, basket);
					goToBasket(ball, place, basket);
				}
			}
			else
			{
				if(ball && place)
				{
					place.unselect();
					place.holder = ball;
					ball.place = place;
					if(place == _selectedPlace)
					{
						_selectedPlace = null;
					}
					
					ball.unselect();
					if(ball == _selectedBall)
					{
						_selectedBall = null;
					}
					
				}
			}
		}
		
		private function goToBasket(ball:Ball, place:Place, basket:Basket)
		{
//			var newPos:Point = ball.view.parent.localToGlobal(ball.getPos());
			//дооолго меняет позицию сцуко
			//			_ballsContainer.addChild(ball.view);
			//			ball.view.x = newPos.x;
			//			ball.view.y = newPos.y;
			//телепортируем в корзину
			Tweener.addTween(ball.view, {delay: Config.BALL_STEP_TIME, 
				onStart: function(ball:Ball, container:DisplayObjectContainer) {var newPos:Point = ball.view.parent.localToGlobal(ball.getPos()); container.addChild(ball.view); ball.view.x = newPos.x; ball.view.y = newPos.y;},
				onStartParams: [ball, _ballsContainer],
				time:Config.BALL_STEP_TIME, 
				x: basket.view.x, 
				y:basket.view.y, 
				onComplete:ballInBasket, 
				onCompleteParams:[ball, basket]}); 
		}
		private function ballInBasket(ball:Ball, basket:Basket) : void
		{
			ball.unselect();
			if(ball == _selectedBall)
			{
				_selectedBall = null;
			}
			removeBall(ball);
			
			if(basket)
			{
				if(basket == _selectedBasket)
				{
					_selectedBasket.unselect();
				}
				basket.unselect();
			}
			
			_basketRun = false;
			_scores++;
			_scoreTF.text = "Score: "+_scores;
			
			recolorizeBaskets();
		}
		
		private function createBalls() : void
		{
			if(_ballsContainer)
			{
				_ballsContainer.parent.removeChild(_ballsContainer);
				_ballsContainer.removeChildren();
			}
			_ballsContainer = new Sprite();
			_startBalls = new Vector.<Ball>();
			addChild(_ballsContainer);
			
			//выставим шары на исходные позиции
			for(var i:int = 0; i<_ballPositions.length; i++)
			{
				recteateBallAtStart(i);
			}
		}
		
		private function recteateBallAtStart(index:int) : void
		{
			var ball:Ball = createBall();
			ball.startPosition = index;
			_startBalls[index] = ball;
			ball.view.x = _ballPositions[index].x;
			ball.view.y = _ballPositions[index].y;
			_ballsContainer.addChild(ball.view);
		}
		
		private function createBall() : Ball
		{
			var b:Ball = new Ball(getRandomColor());
			_freeBalls.push(b);
			var viewClass:Class = getDefinitionByName("BallView") as Class;
			b.view = new viewClass();
			b.view.name = "ball";
			b.addEventListener(Ball.EVENT_CLICK, handleBallClick, false, 0, true);
			return b;
		}
		private function handleBallClick(e:Event):void 
		{
			if (_selectedBall)
			{
				_selectedBall.unselect();
			}
			if (_selectedBall && (_selectedBall == e.target as Ball))
			{
				_selectedBall = null;
			}
			else
			{
				_selectedBall = e.target as Ball;
			}
		}
		private function removeBall(ball:Ball) : void
		{
			if(ball)
			{
				ball.removeEventListener(Ball.EVENT_CLICK, handleBallClick);
				ball.dispose();
				var index:int = _freeBalls.indexOf(ball);
				if (index != -1)
				{
					_freeBalls.splice(index, 1);
				}
			}
		}
		
		protected function update(event:Event):void
		{
			_ring.view.rotation += Config.RING_SPEED;
			_ring.view.rotation %= 360;
			
			if(_lastTime == -1)
			{
				_lastTime = getTimer();
			}
			var time:int = getTimer();
			var delta:Number = time - _lastTime;
			_timer = _timer + delta;
			_lastTime = time;
			_timerTF.text = ""+int((Config.MAX_TIMER - _timer) * 0.001);
			if(_timer >= Config.MAX_TIMER)
			{
				resetGame();
			}
			
			highlightBaskets();
			
//			for each(var ball:Ball in _freeBalls)
//			{
//				ball.update(delta);
//			}
		}
		
		private function highlightBaskets():void 
		{
			unselectBaskets();
			unselectBalls();
			if (_selectedBall )
			{
				if (_selectedBall.startPosition < 0 && _selectedBall.place)
				{
					for each(var basket:Basket in _baskets)
					{
						if (_selectedBall.color == basket.color)
						{
							//поверяем корзину и подсвечиваем, если проверка удалась
							//убираем подсветку, если неудачно
							
							if (getOptimalPath(basket, _selectedBall, true))
							{
								basket.view["highlight"].visible = true;
							}
							else
							{
								basket.view["highlight"].visible = false;
							}
						}
					}
					
				}
				else if (_selectedBall.startPosition >= 0)
				{
					if(!_selectedBall.getPos())
					{
						return;
					}
					//смотрим можем ли мы заскочить в окружность
					var place:Place = getNearestPlace(_selectedBall.getPos());
					//если поблизости нашелся кто-либо
					if (place)
					{
						//смотрим угол между прямыми проведёнными через центр окружности и центры ball и place
						if (checkPlaceAngle(_selectedBall.getPos(), place))
						{
							var path:Vector.<Place> = new Vector.<Place>();
							path.push(place);
							
							if(path && checkPath(path))
							{
								_selectedBall.view["highlight"].visible = true;
							}
						}
					}
				}
			}
		}
		
		private function unselectBalls() : void
		{
			for each(var ball:Ball in _freeBalls)
			{
				ball.view["highlight"].visible = false;
			}
		}
		
		private function unselectBaskets() : void 
		{
			for each(var basket:Basket in _baskets)
			{
				basket.view["highlight"].visible = false;
			}
		}
	}
}